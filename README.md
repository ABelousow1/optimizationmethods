# OptimizationMethods

Library of some numerical method algorithms, implemented for the course of Optimization Methods at KubSU.

## Description
Consists of some the following methods:
- Swann Method to search the interval of indeterminity of unimodal function
- Uniform search for searching the minimum value of unimodal function in range with uniform steps
- Half Division Method for searching the minimum of unimodal function with special kind of binary search with 3 points
- Dichotomy Method for searching the minimum of unimodal function with special kind of binary search with 2 points
- Golden Ratio Method for searching the minimum of unimodal function with division with the golden ratio
- Fibonacci Method for searching the minimum of unimodal function with division with the fibonacci numbers ratio
- Gradient Descent for searching the minimum of multidimensional function using its gradient
- Fastest Descent for searching the minimum of multidimensional function using the maximum of its gradient


All functions implementing methods taking std::function as an argument and a structure of parameters for optimizer.