#include <iostream>
#include <functional>
#include "OptimizationsMethods.h"
#include "..\..\Matr\Matr\Vec.h"
using namespace std;

int main() {
	setlocale(LC_ALL, "Russian");
	function<double(double)> 
		f = [](double x) 
		{ 
			return 2*x*x - 12*x;
		};
	FunctionWithGrad G;
	G.func = [](vec x) { return 2 * x[0] * x[0] + x[0] * x[1] + x[1] * x[1]; };
	G.getGrad = [](vec x) {
		vec y(2);
		y[0] = 4 * x[0] + x[1];
		y[1] = 2 * x[1] + x[0];
		return y;
	};


	double a = 0, b = 0, eps = 0.5, delta = 0.1, t = 1;
	double x=1;
	int N = 5;
	OptimizerParams1D optp{ a, b, x, eps, delta, t, N, 50, true };
	//conf_inter_swann(f, optp);
	//double x;
	//cout << "������� 2x^2 - 2*x + 1.5";
	//uniform_search(f, optp);
	//double minv = dichotomy_opt(f, optp);
	//dichotomy_opt(f, optp);
	//golden_ratio_opt(f, optp);
	//fibonacci_method(f, optp);
	vec V = { 0.5, 1 };
	double e = 0.01, e1 = 0.1, e2 = 0.15, M = 10;
	OptimizationMDParams mdopt{ V, e, e1, e2, 0.5, M };
	gradient_decent(G, mdopt, [](string x) {
		cout << x;
		});
	//cout << format("{}", 123);
	cout << mdopt.start_point;

	//cout << "����� ��������: " << optp.x << endl;


	//cout << "������� �������: " << minv << endl;
	//cout << "� ������������: " << eps;
	//cout << optp.a << " " << optp.b;
	

	return 0;
}