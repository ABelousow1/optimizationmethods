#pragma once
#include <functional>
#include <format>
#include <string>
#include <string_view>
#include <vector>
#include <algorithm>
#include <ranges>
#include "..\..\Matr\Matr\Vec.h"
using namespace std;

using real_func1D = function<double(double)>;

struct OptimizerParams1D {
	double a, b, x, eps, delta, t;
	int N, max_n = 50;
	bool logs = false;
};

using optimizer1D = function<double(real_func1D, OptimizerParams1D)>;
//requiers x - start point
//		a, b - links to begin and end of the interval of indeteminity
//         that will be calculated
//		t - start delta for x
//		logs - to write the logs

int conf_inter_swann(real_func1D f, OptimizerParams1D& data) {
	double &x = data.x, &a = data.a, &b = data.b, &t = data.t;
	bool& logs = data.logs;
	//1 ��������� ������ � ���������� �������
	//2 
	double x_plus_t = x + t, x_minus_t = x - t, delta = t,
		f1 = f(x_minus_t), f2 = f(x), f3 = f(x_plus_t),
		pow_of_2 = 2;
	if (logs) { 
		cout << "������� x_0 = " << x << "\n"; 
		cout << "�������� �������� ������� � ������ x_0 - t = " << x_minus_t
			<< "; x_0 = " << x << "; x_0 + t = " << x_plus_t << ";\n"
			<< "f(" << x_minus_t << ") = " << f1 << ", f(" << x << ") = " << f2 << ", f(" << x_plus_t << ") = " << f3 << endl;
			
	}

	//3 ��������� ������� ���������
	//a)
	if (f1 >= f2 && f2 <= f3) {
		a = x_minus_t;
		b = x_plus_t;
		if (logs) cout << "������ �������� ����������������"
			<< " [" << a << " ; " << b << "]\n";
		return 0;
	}
	//b)
	if (f1 <= f2 && f2 >= f3) {
		if (logs) cout << "����������! ������� �� �����������! :(" <<
			" ������������� ������ ������ ��������� �����\n";
		return 0;
	}
	//4 t
	cout << "������� ��������� �� �����������\n";
	if (f1 >= f2 && f2 >= f3) {
		delta = t;
		a = x;
		cout << "��� ��� f(" << x_minus_t << ") > " << "f(" << x << ") > f(" << x_plus_t << "), �� delta = " << delta << ", a = " << a << ", x1 = x0 + t = " << x << ", k=1" << endl;
		x = x_plus_t;
	}else
		if (f1 <= f2 && f2 <= f3) {
			b = x;
			delta = -t;
			cout << "��� ��� f(" << x_minus_t << ") < " << "f(" << x << ") < f(" << x_plus_t << "), �� delta = " << delta << ", a = " << a << ", x1 = x0 + t = " << x << ", k = 1" << endl;
			x = x_minus_t;
		}
	//5
	double x1 = x + pow_of_2 * delta;
	pow_of_2 *= 2;
	//6
	double f_x1 = f(x1), f_x = f(x);
	cout << "������ ��������� ����� x2 = x1 + 2delta = " << x << " + " << 2*delta << " = " << x1 << endl;
	int k = 1;
	while (!(f_x1 >= f_x)) {
		if (f_x1 < f_x && delta == t) {
			cout << ". ��� ��� f(x" << k << ") = " << f_x1 << " < f(x" << k - 1 << ") � delta = " << t  << ", �� a0 = x" << k - 1 << " = " << x << ". ������� k = " << k + 1 << " � �������� � ���� 5\n";
			a = x;
		}
		if (f_x1 < f_x && delta == -t) {
			cout << ". ��� ��� f(x" << k << ") = " << f_x1 << " < f(x" << k - 1 << ") � delta = " << -t << ", �� b0 = x" << k - 1 << " = " << x << ".\n ������� k = " << k + 1 << " � �������� � ���� 5\n";
			b = x;
		}
		x = x1;
		x1 = x + pow_of_2 * delta;
		f_x = f_x1;
		f_x1 = f(x1);
		pow_of_2 *= 2;
	}
	if (delta == t) {
		b = x1;
	} else
	if (delta == -t) {
		a = x1;
	}
	cout << "��� ��� f(x" << k << ") = " << f_x1 << " > f(x" << k - 1 << ") � delta = " << t << ",\n �� ����� �������� � ��������� �������� ���������������� ����� ��� [" << a << "; " << b << "]\n";
	return 0;

}

double uniform_search_N(real_func1D f, OptimizerParams1D& data) {
	double& x = data.x, & a = data.a, & b = data.b, & t = data.t;
	bool& logs = data.logs;
	int& N = data.N;

	typedef pair<double, double> graphf;
	double delta = (b - a) / N;
	vector <graphf> XsFs(N + 1);
	generate(XsFs.begin(), XsFs.end(), 
		[&delta, &a]() 
		{
			static double p = a - delta;
			return graphf(p+=delta, 0); 
		});
	transform(XsFs.begin(), XsFs.end(), XsFs.begin(), 
		[f, logs](graphf p) 
		{
			auto fm = f(p.first);
			if (logs) {
				cout << "����� � �������� ������� � ���: (" << p.first << "; " << fm << ");\n";
			}
			return graphf(p.first, fm); 
		});
	auto result = min_element(XsFs.begin(), XsFs.end(), 
		[](graphf p1, graphf p2) 
		{ 
			////f(x_i-1)<f(x_i)
			return p1.second < p2.second; 
		});
	auto an = (result-1);
	auto bn = (result + 1);
	if (logs) {
		cout << "����������� �������� " << result->second <<" �� ����������� ��������� � ����� � �����\n" << result->first;
		cout << "\n�� ������� [" << an->first << "; " << bn->first << "]";
	}

	x = result->first;
	return result->second;
}

double uniform_search_eps(real_func1D f, OptimizerParams1D& data) {
	data.N = (int)((data.b - data.a) / (data.eps)-1);
	return uniform_search_N(f, data);
}

double half_division_method(real_func1D f, OptimizerParams1D& data) {
	double& x = data.x, & a = data.a, & b = data.b, & t = data.t, &eps = data.eps;
	bool& logs = data.logs;
	int& N = data.N;

	x = (a + b) / 2;
	double L = (b - a);
	double f_x = f(x);
	int i = 0;
	double Eps = eps * 2;
	while (L >= Eps) {
		double Lo4 = L / 4;
		double y = a + Lo4, z = b - Lo4;
		double f_y = f(y), f_z = f(z);
		if (logs) {
			cout << "��� " << i << endl;
			cout << "y: " << y << "-> f(y): " << f_y << endl;
			cout << "x: " << x << "-> f(x): " << f_x << endl;
			cout << "z: " << z << "-> f(z): " << f_z << endl;
		}
		if (f_y < f_x) {
			b = x;
			x = y;
			f_x = f_y;
			if (logs) {
				cout << "������� �� ��������� ����� ����������� �����\n";
				cout << "����� �������� [" << a << "; " << b << "]\n";
			}
		}
		else {
			if (f_z < f_x) {
				a = x;
				x = z;
				f_x = f_z;
				if (logs) {
					cout << "f(x) < f(z)\n";
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}
			else {
				a = y;
				b = z;
				if (logs) {
					cout << "f(x) = f(z)\n";
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}
			
		}
		L = b - a;
		++i;
	}
	if (logs) {
		cout << "���������� �����������: " << L/2;
	}
	x = (a + b) / 2;

	return f(x);
}

double dichotomy_opt(real_func1D f, OptimizerParams1D& data) {
	double& x = data.x, & a = data.a, & b = data.b, & t = data.t, & eps = data.eps;
	bool& logs = data.logs;
	int& N = data.N;
	double& delta = data.delta;
	double L = (b - a);
	x = (a + b) / 2;
	if (logs) {
		cout << "a: " << a << endl;
		cout << "b: " << b << endl;
		cout << "delta: " << delta << endl;
		cout << "eps: " << eps << endl;
	}
	int i = 0;
	double Eps = eps * 2;
	while (L >= Eps) {
		x = (a + b) / 2;
		double y = x - delta, z = x + delta;
		double f_y = f(y), f_z = f(z);
		if (logs) {
			cout << "��� " << i << endl;
			cout << "y: " << y << "-> f(y): " << f_y << endl;
			cout << "x: " << x << endl;
			cout << "z: " << z << "-> f(z): " << f_z << endl;
		}
		if (f_y < f_z) {
			b = z;
			if (logs) {
				cout << "f(y) < f(z)\n";
				cout << "����� �������� [" << a << "; " << b << "]\n";
			}
		}
		else {
			if (f_y > f_z) {
				a = y;
				if (logs) {
					cout << "f(y) > f(z)\n";
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}
			else {
				a = y;
				b = z;
				if (logs) {
					cout << "f(y) = f(z)\n";
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}

		}
		L = b - a;
		++i;
	}
	if (logs) {
		cout << "���������� �����������: " << L / 2 << endl;
	}

	x = (b + a) / 2;

	return f(x);
}

double golden_ratio_opt(real_func1D f, OptimizerParams1D& data) {
	double& x = data.x, & a = data.a, & b = data.b, & t = data.t, & eps = data.eps;
	bool& logs = data.logs;
	int& N = data.N;

	double L = (b - a);
	double leps = eps+eps;
	static const double gr = (3-sqrt(5))/2;
	if (logs) {
		cout << "a: " << a << endl;
		cout << "b: " << b << endl;
		cout << "eps: " << eps << endl;
	}
	int i = 0;
	double y = a + gr*(b-a), z = a + b - y;
	double f_y = f(y), f_z = f(z);
	while (L >= leps) {
		if (logs) {
			cout << "��� " << i+1 << endl;
			cout << "y: " << y << "-> f(y): " << f_y << endl;
			cout << "z: " << z << "-> f(z): " << f_z << endl;
		}
		if (f_y < f_z) {	
			if (logs) { cout << "f(y) < f(z) = >\n"; }
			b = z;
			if (logs) {	cout << "b = z(" << z << ")\n";}
			z = y;
			if (logs) {	cout << "z = y(" << y << ")\n";	}
			f_z = f_y;
			if (logs) {cout << "f_z = f_y(" << f_y << ")\n";}
			y = a + b - y;
			if (logs) {	cout << "y = a + b - y(" << y << ")";}
			f_y = f(y);
			if (logs) {	cout << "f_y = f(y)(" << f_y << ")" << endl;}
			if (logs) {	cout << "����� �������� [" << a << "; " << b << "]\n";}
		}
		else {
			if (f_y > f_z) {
				if (logs) cout << "f(y) > f(z) => \n";
				a = y;
				if (logs) { cout << "a = y(" << y << ")\n"; }
				y = z;
				if (logs) { cout << "y = z(" << z << ")\n"; }
				f_y = f_z;
				if (logs) cout << "f_y = f_z(" << f_z << ")";
				z = a + b - z;
				if (logs) cout << "z = a + b - z(" << z << ")";
				f_z = f(z);
				if (logs) {
					cout << "f_z = f(z)(" << f_z << ")" << endl;
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}
			else {
				a = y;
				b = z;
				y = a + gr * (b - a);
				z = a + b - y;
				if (logs) {
					cout << "f(y) = f(z)\n";
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}

		}
		L = b - a;
		++i;
	}
	if (logs) {
		cout << "���������� �����������: " << (b-a)/2 << endl;
	}
	x = (a + b) / 2;
	return f(x);
}

int fibonacci_num(int n) {
	static vector<int> v{ 1, 1 };
	if (n < v.size()) {
		return v[n];
	}
	else {
		v.push_back(fibonacci_num(n - 1) + fibonacci_num(n - 2));
		return v[n];
	}
}
double fibonacci_method(real_func1D f, OptimizerParams1D& data) {
	double& x = data.x, & a = data.a, & b = data.b, & t = data.t, & eps = data.eps, delta = data.delta;
	bool& logs = data.logs;
	int& N = data.N;


	double L = (b - a);
	N = 2;
	double c = L / (2 * eps);
	double Fn_1=1, Fn_2=1, Fn;
	while ((Fn = Fn_1 + Fn_2) < c) {
		Fn_1 = Fn_2;
		Fn_2 = Fn;
		++N;
	}
	if (logs) {
		cout << "a: " << a << endl;
		cout << "b: " << b << endl;
		cout << "eps: " << eps << endl;
	}
	int i = 0;
	double y = a + Fn_1 / Fn * (b - a), z = a + Fn_2 / Fn * (b - a);
	double fy = f(y), fz = f(z);
	N = N - 1;
	for (int k = 0; k < N; ++k) {
		if (logs) {
			cout << "��� " << k << endl;
			cout << "y: " << y << "-> f(y): " << fy << endl;
			cout << "z: " << z << "-> f(z): " << fz << endl;
		}
		if (fy <= fz) {
			b = z;
			z = y;
			fz = fy;
			y = a + b - y;
			fy = f(y);
			if (logs) {
				cout << "f(y)<f(z)\n";
				cout << "����� �������� [" << a << "; " << b << "]\n";
			}
		}
		else {
			if (fy > fz) {
				a = y;
				y = z;
				fy = fz;
				z = a + b - z;
				fz = f(z);
				if (logs) {
					cout << "f(y) > f(z)\n";
					cout << "����� �������� [" << a << "; " << b << "]\n";
				}
			}
		}
		L = b - a;
	}
	if (logs) {
		cout << "��� " << N << endl;
		cout << "x = (a + b + delta) / 2;" << endl;
		cout << "����� �������� [" << a << "; " << b << "]\n";
	}
	x = (a + b + delta) / 2;


	if (logs) {
		cout << "���������� �����������: " << L / 2 << endl;
	}
	return f(x);
}

using vec = Vec<double>;

class FunctionWithGrad {
public:
	function<double(vec&)> func = nullptr;
	function<vec(vec&)> getGrad = nullptr;
	double operator()(vec& x){
		if (func) {
			return func(x);
		}
		return 0;
	}
};

struct OptimizationMDParams {
	vec start_point;
	double eps;
	double eps1;
	double eps2;
	double t_k;
	double M = 50;
	function<double(int)> get_t_k = nullptr;
};


void gradient_decent(FunctionWithGrad f, OptimizationMDParams& params, function<void(string)> log = [](string x) {return; }) {
	//1 � ����������� �������


	bool flag = false;
	auto& x_k = params.start_point;
	auto& eps1 = params.eps1;
	auto& eps2 = params.eps2;
	auto& eps = params.eps;
	auto& M = params.M;
	{log(
		format("1. ������� x_0 = {0:c}\n eps = {1}, eps1 = {2}, eps2 = {3}\n M = {4}\n",
			x_k, eps, eps1, eps2, M)); }
	//2 
	{
		log("2. ������� k = 0\n");
	}
	int k = 0;
	//3
	while (k<M) {
		vec gradX_k = f.getGrad(x_k);
		{log(format(
			"3_{0}. ��������"
			" grad(f(x_{0}))"
			" = {1:c}T\n", k, gradX_k));
		}
		//4
		double norma = abs(gradX_k);
		{log(
			format(
				"4_{}. �������� "
				"||grad(f(x_k)|| = {}", k, norma)); }
		if (norma < eps1) {
			log(format(" < {0}, �������� "
				"�������� ���������\n", eps1));
			return;
		}
		log(format(" > {0}\n"
			, eps1));
		//5	if (k >= M) {return;}
		{
			log(format("5_{0}. �������� ������� k>=M: k = {0} < {1} = M\n", k, M));
		}
			//6 set t_k
		double t_k = params.t_k;
		{
			log(format("6_{0}. t_{0} = {1}\n", k, t_k));
		}
		if (params.get_t_k) {
			t_k = params.get_t_k(k);
		}
		//7
		double f_x_k1, f_x_k;
		vec x_k1;
		int sub_k = 0;
		while(true) {
			x_k1 = x_k + (-t_k * gradX_k);

			//8
			f_x_k1 = f(x_k1);
			f_x_k = f(x_k);
			{
				log(format("7_{0}_{1}. ��������"
					" x_{2} = ({3:c})T - {4}*{5:c}T = {6:c}T;"
					" f(x_{0}) = {7}\n", k, sub_k, k + 1, x_k, t_k, gradX_k, x_k1, f_x_k1));
				log(format("8_{0}_{1}. ������� f(x_{3}) c f(x_{2}) = {4}.", k, sub_k, k + 1, k, f_x_k));

			}
			if ((f_x_k1 - f_x_k < 0)) {
				{
					log(format("����� f(x_{}) < f(x_{}), �������� � ���������� ����\n", k + 1, k));
				}
				break;
			}
			{
				log(format("����� f(x_{0}) > f(x_{1})\n"
				"�����: ������� ��� k = {1} �� �����������.\n"
				, k + 1, k)); 
			}
			t_k = t_k / 2;
			{
				log(format("������� t_{0} = {1}\n", k, t_k));
			}
			sub_k++;
		}
	
		//9
		x_k = x_k1;
		double norm_dx = abs(x_k1 - x_k),
			norm_df = abs(f_x_k1 - f_x_k);
		log(format("9_{0}. �������� ||x_{1} - x_{0}|| � |f(x_{1}) - f(x_{0})|:\n", k, k+1));
		bool cond1 = norm_dx < eps2, cond2 = norm_df < eps2;
		if ( cond1 && cond2) {
			if (flag) {
				return;
			}
			flag = true;
		}
		string s = "||x_{0} - x_{1}|| = {2}";
		log(format(s + ((cond1) ? " < " :" > ") + "{3}; "
			"|f(x_{0}) - f(x_{1})| = {4} " + ((cond2) ? " < " : " > ") + "{5}\n",
			k+1, k, norm_dx, eps2, norm_df, eps2));
		k++;
		
	}
}

void fastest_decent(FunctionWithGrad f, OptimizationMDParams& params, optimizer1D opt, OptimizerParams1D& par1D) {
	//1 � ����������� �������


	bool flag = false;
	auto& x_k = params.start_point;
	auto& eps1 = params.eps1;
	auto& eps2 = params.eps2;
	auto& eps = params.eps;
	auto& M = params.M;
	//2 

	int k = 0;
	//3
	while (k < M) {
		vec gradX_k = f.getGrad(x_k);
		//4
		if (abs(gradX_k) < eps1) {
			return;
		}
		//5	if (k >= M) {return;}
		//6 set t_k
		auto h = [f, gradX_k, x_k](double t) mutable{
			vec y = x_k + (-t) * gradX_k;
			return f(y);
		};
		opt(h, par1D);
		auto t_k = par1D.x;
		//7
		auto x_k1 = x_k + (-t_k * gradX_k);
		//8
		auto f_x_k1 = f(x_k1);
		auto f_x_k = f(x_k);
		if (!(f_x_k1 - f_x_k < 0)) {
			return;
		}
		//9
		x_k = x_k1;

		if (abs(x_k1 - x_k) < eps2 && abs(f_x_k1 - f_x_k) < eps2) {
			if (flag) {
				return;
			}
			flag = true;
		}
		k++;

	}
}


//class general_optimizer {
//	double a, b, x;
//	function<double(double)> f;
//public:
//	general_optimizer concrete_
//	int optimize(function<double(double)> f, general_optimizer g) {
//		
//	}
//};
class optimizer {
	virtual void init_optimization() = 0;
	virtual void start_cycle() = 0;
	virtual void comparison() = 0;
	virtual void set_less_greater_action() = 0;
	virtual bool set_stop_condition() = 0;
	void optimize() {
		init_optimization();

		start_cycle();
	}
};

